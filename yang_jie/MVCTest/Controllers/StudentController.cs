﻿using System;
using MVCTest.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCTest.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            var list = new List<Student>();

            for (int i = 0; i<10; i++)
            {
                list.Add(new Student { Name = string.Format("学生{0}", i), Age = 19 });
            }
          
            return View(list);
        }
    }
}