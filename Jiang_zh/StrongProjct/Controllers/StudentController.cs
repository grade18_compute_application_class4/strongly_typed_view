﻿using StrongProjct.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StrongProjct.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            var list = new List<Student>();

            for (int i = 1; i < 6; i++)
            {
                var StuName = new Student { Name = "学生" + i + "", Age = "20" };
                list.Add(StuName);

            }
            //ViewBag.list = list;
            return View(list);
        }
    }
}