﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.message = "关于一个可视化操作的界面,在网页当中视图是指用户看到并与之交互的界面";
            ViewBag.Rbac = "意思是基于角色访问控制";
            ViewBag.Denefit = "MVC的好处之一在于它能为应用程序处理很多不同的试图" +
                "在试图中其实没有真正的处理发生，他只是作为一种输出数据并允许用户操作的方式";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();

        }

        public ActionResult wang()
        {
            return View("About");
        } 
    }
}