﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace luo_l.Controllers
{
    public class BbaController : Controller
    {
        // GET: Bba
        public ActionResult Index()
        {
            ViewBag.Title = "我爱她轰轰烈烈";
            return View();
        }
        public ActionResult List()
        {
            List<Student> students = new List<Student>();

            for (int i=0;i<=5;i++)
            {
                var stu = new Student { Name = string.Format("学生{0}号", i), Age = 15 };
                students.Add(stu);
            }
            // return View("Index" );
            ViewBag.List = students;
            return View();
        }
        
    }
    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }

    }
    

}