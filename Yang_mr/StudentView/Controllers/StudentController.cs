﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentView.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            ViewBag.TitleAbout = "木头的心";

            return View();
        }

        public ActionResult List()
        {
            //return View("Index");

            //return View("~/Views/Home/Student.cshtml");
            List<Student> studentList = new List<Student>();
            for(int i = 0; i < 5; i++)
            {
                var stu = new Student { Name = string.Format("学生{0}", i), Age = 20 };
                studentList.Add(stu);
            }
            ViewBag.list = studentList;
            return View();
        }
    }
    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}