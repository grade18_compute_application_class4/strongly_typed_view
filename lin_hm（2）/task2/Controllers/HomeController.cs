﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace task2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Title = "林华铭";

            ViewBag.Message = "测试一";

            ViewBag.Rbac = "测试三";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Titlle = "谢汶霖";

            ViewBag.Message = "测试二";

            return View();
        }

        public ActionResult BBC()
        {
            return View("About");
        }
    }
}