﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace task2.Controllers
{
    public class rankController : Controller
    {
        // GET: Bilibili
        public ActionResult Index()
        {
            ViewBag.student = "学生信息";
            return View();
        }

        public ActionResult task()
        {
            return View("Index");
        }

        public ActionResult top()
        {
            return View("~/views/rank/Index.cshtml");
        }

        public ActionResult list()
        {
            List<Student> studentList = new List<Student>();

            for (int i = 1; i <11; i++)
            {
                var stu = new Student { Name = string.Format("学生编号{0}", i), Age = 100 };
                studentList.Add(stu);
            }

            ViewBag.list = studentList;

            return View();
        }
    }
    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}