﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }


        public ActionResult Contact()
        {
            ViewBag.Title = "联系方式";

            ViewBag.Message = "Your contact page.";

            ViewBag.Rbac = "老王";

            return View();
        }

        public ActionResult Wang()
        {
            return View("About");
        }
    }
}